# hypo-hypocampus-api

Python API for interacting with hypocampus REST APIs.

Installation
============

This is not available as a pypi installation but can be installed directly from gitlab using:

pip install  git+https://gitlab.com/magnus.odman/hypo-hypocampus-api.git


