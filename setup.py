
import setuptools
with open("README.md", "r") as fh:
    long_description = fh.read()
setuptools.setup(
     name='hypocampus',  
     version='0.1',
     scripts=[] ,
     author="Magnus Ödman",
     author_email="magnus.odman@gmail.com",
     description="A wrapper around the hypocampus REST API",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://hub.com/javatechy/dokr",
     packages=setuptools.find_packages(),
     install_requires=[
          'PyJWT',
          'requests'
     ],
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
)
