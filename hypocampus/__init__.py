import sys
import requests
import jwt
import time
import datetime
from os import getenv
from enum import Enum
from datetime import timedelta
from functools import lru_cache

class Hypocampus:
    
    def __init__(self, server_url):
        self.server_url = server_url
        
    def get_token(self, email, password):
        url = "{}/api/v1/authentication/local".format(self.server_url)
        headers = {
            "content-type": "application/json; charset=UTF-8",
            "cache-control": "no-cache",
            "authority": "test.hypocampus.se",
        }
        data='{{"email":"{}","password":"{}","remainLoggedIn":false}}'.format(email, password)
        response = requests.post(url, data=data)
        assert response.status_code == 200
        token = response.json()["webToken"]
        return token
    
    def get_analytics_token(self, token, course_id):
        url = '{}/api/v2/analytics/token/course/{}'.format(self.server_url, course_id)
        headers = {
            'pragma': 'no-cache',
            'cache-control': 'no-cache',
            'authorization': 'Bearer {}'.format(token)
        }

        response = requests.get(url, headers=headers)
        assert response.status_code == 200
        analytics_token = response.json()["webToken"]
        return analytics_token
    
    @lru_cache(maxsize=32)
    def get_points_for_course(self, course_id, analytics_token):
        url = '{}/api/v2/analytics/course/{}/points'.format(self.server_url, course_id)
        headers = {
            "authorization": "Bearer {}".format(analytics_token)
        }
        response = requests.get(url, headers=headers)
        print(response)
        assert response.status_code == 200
        return response.json()

    @lru_cache(maxsize=32)
    def get_chaptergroups_for_course(self, analytics_token, course_id):
        print("Fetching chapters for course: {}".format(course_id))
        url = '{}/api/v2/analytics/course/{}/chapterGroups'.format(self.server_url, course_id)
        headers = {
            "authorization": "Bearer {}".format(analytics_token)
        }
        response = requests.get(url, headers=headers)
        assert response.status_code == 200
        return response.json()
    
    @lru_cache(maxsize=32)
    def get_chapters_for_course(self, analytics_token, course_id):
        print("Fetching chapters for course: {}".format(course_id))
        url = '{}/api/v2/analytics/course/{}/chapters'.format(self.server_url, course_id)
        headers = {
            "authorization": "Bearer {}".format(analytics_token)
        }
        response = requests.get(url, headers=headers)
        assert response.status_code == 200
        return response.json()
    
    def get_weekly_report_users(self, analytics_token):
        url= "{}/api/v2/analytics/course/weeklyreport/users".format(self.server_url)
        print(url)
        headers = {
            "authorization": "Bearer {}".format(analytics_token)
        }
        response = requests.get(url, headers=headers)
        assert response.status_code == 200
        return response.json()

class Analytics:
    
    def __init__(self, jwt_secret="TEST_SECRET"):
        self.jwt_secret = jwt_secret

    def get_analytics_token_for_course(self, course_id: int):
        payload = {"course": course_id, "jti": "99999999","sub": "user", "iss": "https://hypocampus.se/", "roles": "system", "iat": int(time.time()), "exp": int(time.time() + 3 * 3600)}
        return jwt.encode(payload, self.jwt_secret, algorithm="HS256").decode("UTF-8")
    
    def get_course_stats_for_user(self, analytics_token, course_id, user_id, from_date="1970-01-01", to_date="2999-01-01"):
        url = "https://yah6yvscfh.execute-api.eu-central-1.amazonaws.com/prod/userstudyplan/{}/{}/{}/{}".format(course_id, user_id, from_date, to_date)
        #print(url)
        headers = {
            "Authorization": "Bearer {}".format(analytics_token)
        }
        response = requests.get(url, headers=headers)
        print(response)
        assert response.status_code == 200
        return response.json()
        
    def get_analytics_token_for_course(self, course_id: int):
        payload = {
            "course": course_id,
            "jti": "99999999",
            "sub": "user",
            "iss": "https://hypocampus.se/",
            "roles": "system,teacher",
            "iat": int(time.time()),
            "exp": int(time.time() + 3 * 3600)
        }
        return jwt.encode(payload, self.jwt_secret, algorithm="HS256").decode("UTF-8")
    
    def get_analytics_token_for_system(self):
        payload = {
            "jti": "99999999",
            "sub": "user",
            "iss": "https://hypocampus.se/",
            "roles": "system,teacher",
            "iat": int(time.time()),
            "exp": int(time.time() + 3 * 3600)
        }
        return jwt.encode(payload, self.jwt_secret, algorithm="HS256").decode("UTF-8")

name="hypocampus"
